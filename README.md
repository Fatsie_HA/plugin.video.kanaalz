kanaal Z Videozone for Kodi
===========================

Kodi plugin to watch videos from kanaal Z.

Usage
-----

Grab a zip, fire up Kodi's settings, and select *Install from zip file*.
After installation, the plugin will be listed under your video add-ons.

Author
------

[Staf Verhaegen](mailto:staf@stafverhaegen.be)

License
-------

Copyright &copy; 2015 Staf Verhaegen

This software is released under the GNU GPL V3 license.
